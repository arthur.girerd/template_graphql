var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');
var morganBody = require('morgan-body');
var bodyParser = require('body-parser');
const graphql = require('graphql');
const { activity, apps, brands } = require('./data/index.js');
var { morgan } = require('morgan');
var { nanoid } = require('nanoid')
const {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLFloat,
  GraphQLList
} = graphql

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    hello: [toto],
    activity: [Activity],
    brands: [Brand],
    apps: [App]
    user : String
  }

  type toto {
    name: String
    date: String
  }

  type Brand {
    uuid:String
    name:String
}


type App {
    uuid:String
    name:String
}

  type Activity {
    uuid:String
    brand_uuid:String
    app_uuid:String
    timestamp:String
}




`);
var morgan = require('morgan')



// The root provides a resolver function for each API endpoint
var root = {
  hello: () => {
    return [{ "name": "Arthur", "date": "2022-03-01" }, { "name": "Romain", "date": "2022-04-01" }];
  },
  activity: () => {
    return activity;
  },
  brands: () => {
    return brands;
  }
  ,
  apps: () => {
    return apps;
  }

};

var app = express();
morgan.token('body', req => {
  return JSON.stringify(req.body)
})
app.use(morgan(':method :url :body'));
app.use(bodyParser.json());
app.use(function (req, res, next) {

  var host = req.headers;
  console.log(host);
  next();
})
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
  context: ({ req }) => {
    console.log(req.headers.authorization)

    return req
  },
}));
app.listen(4000);
//morganBody(app);
console.log('Running a GraphQL API server at http://localhost:4000/graphql');