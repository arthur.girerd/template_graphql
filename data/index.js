var { nanoid } = require('nanoid')
const { faker } = require('@faker-js/faker');

const brand1 = { uuid: 'gH70XF_1y-L5YDQ2xke_r', name: 'Total 1', price: '1.99' }
const brand2 = { uuid: 'wTU3S8h86WDDUtfZ9l_3b', name: 'Total 2', price: '1.99' }
const brands = [brand1, brand2]

const app1 = { uuid: 'KUgDya1dctQANb0t6NM6V', name: 'APP 1', price: '1.99' }
const app2 = { uuid: 'pz-By8PGDmlD6LmKhUVTQ', name: 'APP 2', price: '1.99' }
const app3 = { uuid: 'CGTM5mxdZJcO1JXrt2_r1', name: 'APP 3', price: '1.99' }
const apps = [app1, app2, app3]

let rand = randomIntFromInterval(5, 1000);

let activity = [];
while (rand > 0) {
    rand--;
    activity.push(
        {
            uuid: nanoid(),
            brand_uuid: brands[randomIntFromInterval(0, 1)].uuid,
            app_uuid: apps[randomIntFromInterval(0, 2)].uuid,
            timestamp: new Date(+(new Date()) - Math.floor(Math.random() * 10000000000)),
        })
}


function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
}

module.exports = { activity, brands, apps }
